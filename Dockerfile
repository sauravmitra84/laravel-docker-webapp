# Set the Base Image to Ubuntu
FROM ubuntu:bionic

############################################################
# Dockerfile to build Laravel Webapp container images
# Specifically for Blog
# Nginx, PHP-FPM, MEMCACHED, REDIS
# Based on Ubuntu 18.04 bionic LTS
# Written by Saurav Mitra (saurav.karate@gmail.com)
############################################################
# Build Command:
# docker build --compress --no-cache --tag laravel-docker-webapp:1.0 --file Dockerfile .
# Run Command:
# docker container run \
# --name blog1.com \
# --expose 80 \
# --expose 443 \
# --net nginx-proxy \
# -e VIRTUAL_HOST=blog1.com \
# --mount type=bind,source=/root/workspace/blog1.com/srv,target=/srv \
# --mount type=bind,source=/root/workspace/blog1.com/redis,target=/var/lib/redis \
# --mount type=bind,source=/root/workspace/blog1.com/log,target=/var/log \
# -d laravel-docker-webapp:1.0
# Execute Command:
# docker container exec -it laravel-docker-webapp:1.0 bash

LABEL version="1.0" maintainer="Saurav.Karate@gmail.com" description="Dockerfile to build Laravel Webapp container images. \
Warning: Private Image, Copyrighted; Not for public use."

RUN apt-get --assume-yes --quiet update \
    && apt-get --assume-yes --quiet install locales \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ARG LANG='en_US.utf8'

# Generic Default Parameters & Variables
ARG SITENAME='webapp.demo'
ARG SITEUSER='webapp'
ARG WEBROOT='/srv'
ARG PHP_SERVER_CONFIG='/etc/php/7.2/fpm/php.ini'
ARG PHP_POST_MAX_SIZE='4M'



# Install Nginx
RUN apt-get --assume-yes --quiet install nginx
# Configure Nginx    
RUN mkdir -p /opt/skel/public \
	&& useradd -b ${WEBROOT} -d ${WEBROOT}/${SITENAME} -m -k /opt/skel -s /bin/false ${SITEUSER} \
	&& chmod 755 ${WEBROOT}/${SITENAME}
# Nginx Base Configuration
COPY ./nginx.conf /etc/nginx/
# Adjusting buffer allocation by nginx for FastCGI
COPY ./fastcgi_params /etc/nginx/
# Webapp.demo site configuration
COPY ./webapp.demo /etc/nginx/sites-available/
COPY ./gzip.conf /etc/nginx/conf.d/
RUN mkdir /var/log/webapp.demo \
	&& rm -f /etc/nginx/sites-enabled/default \
    && rm -f /etc/nginx/sites-available/default \
    && ln -sf /etc/nginx/sites-available/${SITENAME} /etc/nginx/sites-enabled/${SITENAME} \
	&& rm -rf /etc/systemd/system/multi-user.target.wants/nginx.service \
	&& rm -rf /etc/init.d/nginx



# Install PHP-FPM, Memcached
RUN apt-get --assume-yes --quiet install php-fpm php-gd php-apcu php-zip php-cli php-curl php-mysql \
    && apt-get --assume-yes --quiet install memcached php-memcached \
    && apt-get --assume-yes --quiet install curl unzip php-mysql php-mbstring php-xml php-zip php-json php-pdo php-bcmath php-imap php-intl php-soap php-pdo-pgsql php-pgsql php-pdo-sqlite php-sqlite3
# Install Composer
RUN cd ${WEBROOT}/${SITENAME} \
	&& curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


# Configure PHP-FPM
RUN sed -i "s/^post_max_size =.*$/post_max_size = ${PHP_POST_MAX_SIZE}/"              	$PHP_SERVER_CONFIG \
	&& sed -i "s/^.*session.save_handler = .*$/session.save_handler = memcached/g"      $PHP_SERVER_CONFIG \
	&& sed -i "s~^;session.save_path =.*$~session.save_path = /tmp/memcached.sock:0~"   $PHP_SERVER_CONFIG
# Configure APC PHP's Opcode Cache
COPY ./apcu.ini /etc/php/7.2/mods-available/
# Configure PHP-FPM Pool
COPY ./php-fpm.conf /etc/php/7.2/fpm/
COPY ./default.conf /etc/php/7.2/fpm/pool.d/
# Configure Memcached
COPY ./memcached.conf /etc/ 
RUN mkdir /var/log/php /var/log/memcached \
	&& rm -rf /etc/php/7.2/fpm/pool.d/www.conf \
	&& rm -rf /etc/systemd/system/multi-user.target.wants/php7.2-fpm.service \
	&& rm -rf /etc/systemd/system/timers.target.wants/phpsessionclean.timer \
	&& rm -rf /etc/init.d/php7.2-fpm \
	&& rm -rf /etc/systemd/system/multi-user.target.wants/memcached.service \
	&& rm -rf /etc/init.d/memcached



# Install Redis
RUN apt-get --assume-yes --quiet install redis-server
COPY ./redis.conf /etc/redis/
RUN rm -rf /etc/systemd/system/multi-user.target.wants/redis-server.service \
	&& rm -rf /etc/init.d/redis-server



# Install Supervisor
RUN apt-get --assume-yes --quiet install supervisor \
    && apt-get clean && rm -rf /var/lib/apt/lists/*
COPY ./supervisord.conf /etc/supervisor/conf.d/



# Finally
WORKDIR ${WEBROOT}/${SITENAME}
VOLUME ["${WEBROOT}", "/var/log", "/var/lib/redis"]
EXPOSE 80 443
ENTRYPOINT ["/usr/bin/supervisord"]
